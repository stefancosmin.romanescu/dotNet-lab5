﻿using System;
using System.Collections.Generic;
using Business;
using Lab5.DTO;
using Microsoft.AspNetCore.Mvc;
using Model;

namespace Lab5.Controllers
{
    [Route("api/[controller]")]
    public class CarsController : Controller
    {
        private readonly ICarRepository _repository;

        public CarsController(ICarRepository repository)
        {
            _repository = repository;
        }

        // GET api/cars
        [HttpGet]
        public IEnumerable<Car> Get()
        {
            return _repository.GetAllCars();
        }

        // GET api/cars/5
        [HttpGet("{id}")]
        public Car Get(Guid id)
        {
            return _repository.GetCarById(id);
        }

        // GET api/cars/electic
        [HttpGet("electric")]
        public IEnumerable<Car> GetElectic()
        {
            return _repository.GetElectricCars();
        }

        // POST api/cars
        [HttpPost]
        public StatusCodeResult Post([FromBody]CreateCarDto car)
        {
            var entity = Car.Create(car.Name, car.IsElectric);
            var result = _repository.AddCar(entity);
            return StatusCode(result ? 201 : 409);
        }

        // PUT api/cars/5
        [HttpPut("{id}")]
        public void Put(Guid id, [FromBody]UpdateCarDto car)
        {
            var entity = _repository.GetCarById(id);
            entity.Update(car.Name, car.IsElectric);
            _repository.EditCar(entity);
        }

        // DELETE api/cars/5
        [HttpDelete("{id}")]
        public StatusCodeResult Delete(Guid id)
        {
            var result = _repository.DeleteCar(id);
            return StatusCode(result ? 200 : 404);
        }
    }
}
