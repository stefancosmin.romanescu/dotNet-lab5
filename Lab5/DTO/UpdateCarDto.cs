﻿using System;

namespace Lab5.DTO
{
    public class UpdateCarDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public bool IsElectric { get; set; }
    }
}
