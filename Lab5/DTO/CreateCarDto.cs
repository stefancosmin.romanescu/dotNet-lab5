﻿namespace Lab5.DTO
{
    public class CreateCarDto
    {
        public string Name { get; set; }

        public bool IsElectric { get; set; }
    }
}
