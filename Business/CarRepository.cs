﻿using System;
using System.Collections.Generic;
using System.Linq;
using Context;
using Model;

namespace Business
{
    public class CarRepository: ICarRepository
    {
        private readonly IDatabaseService _databaseService;

        public CarRepository(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public bool AddCar(Car car)
        {
            _databaseService.Cars.Add(car);
            _databaseService.SaveChanges();
            return true;
        }

        public void EditCar(Car car)
        {
            _databaseService.Cars.Update(car);
            _databaseService.SaveChanges();
        }

        public bool DeleteCar(Guid id)
        {
            var foundCar = GetCarById(id);
            if (foundCar is null) return false;
            _databaseService.Cars.Remove(foundCar);
            _databaseService.SaveChanges();
            return true;
        }

        public IReadOnlyList<Car> GetAllCars()
        {
            return _databaseService.Cars.ToList();
        }

        public Car GetCarById(Guid id)
        {
            return _databaseService.Cars.FirstOrDefault(car => car.Id == id);
        }

        public IReadOnlyList<Car> GetElectricCars()
        {
            return _databaseService.Cars.Where(car => car.IsElectric).ToList();
        }
    }
}
