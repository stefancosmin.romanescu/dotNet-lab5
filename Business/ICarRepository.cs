﻿using System;
using System.Collections.Generic;
using Model;

namespace Business
{
    public interface ICarRepository
    {
        bool AddCar(Car car);
        void EditCar(Car car);
        bool DeleteCar(Guid id);
        IReadOnlyList<Car> GetAllCars();
        Car GetCarById(Guid id);
        IReadOnlyList<Car> GetElectricCars();

    }
}
